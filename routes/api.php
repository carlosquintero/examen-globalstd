<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Api calls publicas
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login')->name("login");
});

// Api calls que requieren sesión iniciada
Route::group(['middleware' => 'auth:api'], function () {
    // User api calls
    Route::group(['prefix' => 'auth'], function () {
        Route::get('get', 'AuthController@getInfoUser');
        Route::get('logout', 'AuthController@logout');
    });
    // Peliculas api calls
    Route::group(['prefix' => 'movies'], function () {
        Route::get('get', 'PeliculaController@get');
        Route::post('store', 'PeliculaController@store');
        Route::get('visibility/{id}', 'PeliculaController@visibility')->where('id', '[0-9]+');
        Route::delete('delete/{id}', 'PeliculaController@delete')->where('id', '[0-9]+');
        Route::post('update/{id}', 'PeliculaController@update')->where('id', '[0-9]+'); // checar workaround para que funcionen los archivos en PUT
        Route::group(['prefix' => 'horarios'], function () {
            Route::get('get/{id}', 'PeliculaController@horarios')->where('id', '[0-9]+');
            Route::post('store', 'PeliculaController@storeHorarios');
        });
    });
    // Turnos api calls
    Route::group(['prefix' => 'turnos'], function () {
        Route::get('get', 'TurnoController@get');
        Route::post('store', 'TurnoController@store');
        Route::get('visibility/{id}', 'TurnoController@visibility')->where('id', '[0-9]+');
        Route::delete('delete/{id}', 'TurnoController@delete')->where('id', '[0-9]+');
        Route::put('update/{id}', 'TurnoController@update')->where('id', '[0-9]+');
    });
    // Usuarios api calls
    Route::group(['prefix' => 'users'], function () {
        Route::get('get', 'UserController@get');
        Route::post('store', 'UserController@store');
        Route::delete('delete/{id}', 'UserController@delete')->where('id', '[0-9]+');
        Route::put('update/{id}', 'UserController@update')->where('id', '[0-9]+');
    });
});
