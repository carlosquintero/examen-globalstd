<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'imagen', 'fecha_publicacion', 'user_id', 'activo'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'fecha_publicacion' => 'datetime:Y-m-d',
    ];

    public function horarios () {
        return $this->hasMany('App\PeliculasTurno', 'movie_id', 'id');
    }
}
