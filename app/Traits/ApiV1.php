<?php


namespace App\Traits;


trait ApiV1
{

    public $errors;

    public $data_response;

    public $request_result;

    public $validation_errors = false;


    public function validator()
    {

        if (isset($this->validation_rules)) {

            $validator = \Validator::make($this->request->all(), $this->validation_rules);

            if ($validator->fails()) {

                $this->handleError($validator->errors());

                $this->validation_errors = true;

                return false;
            }

            return true;
        }

        return true;
    }


    public function response()
    {

        $this->request_result = $this->request_result ??
            !$this->errors ? true : false;

        return \Response::json([
            "resquest" => $this->request_result,
            "data" => $this->data_response,
            "errors" => $this->errors,
        ], $this->errors ? 500 : 200);

    }

    public function setDataResponse($data = null)
    {
        $this->data_response = $data;
    }

    public function handleError($errors)
    {
        $this->errors = $errors;

        $this->request_result = false;

        $this->data_response = null;
    }

}
