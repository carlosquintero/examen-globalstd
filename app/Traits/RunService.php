<?php


namespace App\Traits;


/**
 * Trait RunService
 * Este trait detaermina la funcion que debe ser llamada del servicio
 * y luego la executa
 *
 * @package App\Traits
 */
trait RunService
{

    protected $controller_request;


    /**
     * Este metodo es el que realiza el trabajo del trait.
     * Obtiene el controlador y el metodo de la ruta luego
     * llama con peffix de los servicos
     */
    function runService(): void
    {

        $this->controller_request = \Route::currentRouteAction();

        $request_function = explode("@", $this->controller_request)[1] ?? null;


        /*
         * Englobamos el try-catch para todos los servicios
         * para evitar duplicar el codigo dentro de los
         * metodos
         */
        try {

            $this->{"_" . $request_function}();

        } catch (\Throwable $th) {

            $this->handleError($th->getMessage());

        }

    }


    /**
     * Este metodo magico ejecuta el controllador aunque
     * el metodo definido en la ruta no exista
     *
     * @param $method
     * @param $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->response();
    }

}
