<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'horario', 'activo'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'horario' => 'datetime:H:i:s',
    ];

    /**
     * timestamps for the model.
     *
     * @var boolean
     */
    public $timestamps = true;
}
