<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeliculasTurno extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_movie', 'turno_id'
    ];

    /**
     * timestamps for the model.
     *
     * @var boolean
     */
    public $timestamps = false;
}
