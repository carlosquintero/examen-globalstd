<?php

namespace App\Http\Controllers;

use App\Turno;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TurnoController extends ControllerApi
{
    /**
     * @var array
     */
    public $validation_rules = [];

    public function get() {
        $this->setDataResponse(Turno::orderBy('horario', 'asc')->get());
        return $this->response();
    }

    public function delete($id) {
        $turno = Turno::where('id', $id)->first();
        if($turno === null) {
            $this->handleError(["general" => "No existe el turno."]);
            return $this->response();
        }
        if ($turno->delete()) {
            $this->setDataResponse(true);
        } else {
            $this->handleError(["general" => "Ocurrio un problema al eliminar en la base de datos."]);
            return $this->response();
        }
        return $this->response();
    }

    public function visibility($id) {
        $turno = Turno::where('id', $id)->first();
        if($turno === null) {
            $this->handleError(["general" => "No existe el turno."]);
            return $this->response();
        }
        $turno->activo = ($turno->activo) ? false : true;
        if ($turno->save()) {
            $this->setDataResponse(true);
        } else {
            $this->handleError(["general" => "Ocurrio un problema al modificar en la base de datos."]);
            return $this->response();
        }
        return $this->response();
    }

    public function update() {
        $this->validation_rules = [
            "id" => "required|integer|exists:App\Turno,id",
            "horario" => "required|date_format:H:i:s",
            "activo" => "required"
        ];
        if (!$this->validator()) {
            return $this->response();
        }
        $turno = Turno::where('id', $this->id)->first();
        if ($this->request->horario !== $turno->horario->format('H:i:s') && Turno::where('horario', $this->request->horario)->exists()) {
            $this->handleError(["general" => "El horario ya existe."]);
            return $this->response();
        }
        $turno->fill($this->request->all());
        if ($turno->save()) {
            $this->setDataResponse(true);
        } else {
            $this->handleError(["general" => "Ocurrio un problema al guardar en la base de datos."]);
            return $this->response();
        }
        return $this->response();
    }

    public function store()
    {
        $this->validation_rules = [
            "horario" => "required|date_format:H:i:s",
            "activo" => "required"
        ];
        if (!$this->validator()) {
            return $this->response();
        }
        if (Turno::where('horario', $this->horario)->exists()) {
            $this->handleError(["general" => "El horario ya existe."]);
            return $this->response();
        }
        $turno = new Turno();
        $turno->user_id = Auth::id();
        $turno->fill($this->request->all());
        if ($turno->save()) {
            $this->setDataResponse(true);
        } else {
            $this->handleError(["general" => "Ocurrio un problema al guardar en la base de datos."]);
            return $this->response();
        }
        return $this->response();
    }
}
