<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\User;
use App\UsuarioInfo;
use App\Capilaridad;
use Illuminate\Support\Facades\Auth;

class AuthController extends ControllerApi
{

    /**
     * @var array
     */
    public $validation_rules = [];

    /**
     * @return mixed
     */
    public function login($tipo = 'embajador')
    {
        $this->validation_rules = [
            "email" => "required|email",
            "password" => "required|string",
        ];
        $credentials = [
            'email' => $this->email,
            'password' => $this->password
        ];
        if (!$this->validator()) {
            return $this->response();
        }
        if (!Auth::attempt($credentials)) {
            $this->handleError(["general" => "Tus datos de acceso no son correctos."]);
            return $this->response();
        }
        $user = $this->request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $this->setDataResponse([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            "user_data" => $user
        ]);
        return $this->response();
    }

    public function logout() {
        $user = Auth::user()->token();
        $user->revoke();
        $this->setDataResponse(true);
        return $this->response();
    }

    public function getInfoUser() {
        $this->setDataResponse(
            User::find(Auth::id())
        );
        return $this->response();
    }

    public function update() {
        /*
        'telefono' => 'string',
            'movil' => 'string',
            'genero' => 'In:F,M',
            'foto' => 'File',
            'rfc' => 'String',
            'talla' => 'string',
            'nacimiento' => 'date|date_format:Y-m-d',
        */
        $this->validation_rules = [
            'nombre' => 'required|string',
        ];
        if (!$this->validator()) {
            return $this->response();
        }
        $user = $this->request->user();
        $user->nombre = $this->nombre;
        if($user->save()) {
            $campos = $this->request->all();
            $updateFields = $user->info->toArray();
            foreach($campos as $campo => $valor) {
                if (trim($valor) !== "") {
                    $updateFields[$campo] = $valor;
                }
            }
            if ($this->request->hasFile('foto')) {
                $updateFields['foto'] = basename($this->request->foto->store('usuarios', 'public'));
            }
            UsuarioInfo::updateOrCreate(['usuario' => $user->id], $updateFields);
            $this->setDataResponse(User::find(Auth::id()));
        } else {
            $this->setDataResponse(false);
        }
        return $this->response();
    }
}
