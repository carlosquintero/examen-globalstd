<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends ControllerApi
{
    /**
     * @var array
     */
    public $validation_rules = [];

    public function get() {
        $this->setDataResponse(User::all());
        return $this->response();
    }

    public function delete($id) {
        $user = User::where('id', $id)->first();
        if($user === null) {
            $this->handleError(["general" => "No existe el usuario."]);
            return $this->response();
        }
        if ($user->delete()) {
            $this->setDataResponse(true);
        } else {
            $this->handleError(["general" => "Ocurrio un problema al eliminar en la base de datos."]);
            return $this->response();
        }
        return $this->response();
    }

    public function update() {
        $this->validation_rules = [
            "id" => "required|integer|exists:App\User,id",
            "name" => "required|string",
            "email" => "required|email",
        ];
        if (!$this->validator()) {
            return $this->response();
        }
        $campos = $this->request->all();
        unset($campos['password']);
        $user = User::where('id', $this->id)->first();
        if ($this->request->email !== $user->email && User::where('email', $this->request->email)->exists()) {
            $this->handleError(["general" => "El email ya se encuentra registrado."]);
            return $this->response();
        }
        if ($this->request->filled('password')) {
            $campos['password'] = Hash::make($this->request->password);
        }
        $user->fill($campos);
        if ($user->save()) {
            $this->setDataResponse(true);
        } else {
            $this->handleError(["general" => "Ocurrio un problema al guardar en la base de datos."]);
            return $this->response();
        }
        return $this->response();
    }

    public function store()
    {
        $this->validation_rules = [
            "name" => "required|string",
            "email" => "required|email",
            "password" => "required|string",
        ];
        if (!$this->validator()) {
            return $this->response();
        }
        if (User::where('email', $this->request->email)->exists()) {
            $this->handleError(["general" => "El email ya se encuentra registrado."]);
            return $this->response();
        }
        $campos = $this->request->all();
        $user = new User();
        $campos['password'] = Hash::make($campos['password']);
        $user->fill($campos);
        if ($user->save()) {
            $this->setDataResponse(true);
        } else {
            $this->handleError(["general" => "Ocurrio un problema al guardar e la base de datos."]);
            return $this->response();
        }
        return $this->response();
    }
}
