<?php

namespace App\Http\Controllers;

use App\Pelicula;
use App\PeliculasTurno;
use App\Turno;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PeliculaController extends ControllerApi
{
    /**
     * @var array
     */
    public $validation_rules = [];

    public function get() {
        $this->setDataResponse(Pelicula::orderBy('id', 'asc')->get());
        return $this->response();
    }

    public function horarios($id) {
        $pelicula = Pelicula::with(['horarios'])->where('id', $id)->first();
        if($pelicula === null) {
            $this->handleError(["general" => "No existe la película."]);
            return $this->response();
        }
        $asignados = $pelicula->horarios->pluck('turno_id');
        $this->setDataResponse([
            'horarios_disponibles' => Turno::select('id', 'horario')->where('activo', true)->whereNotIn('id', $asignados)->orderBy('horario', 'asc')->get(),
            'horarios_asignados' => Turno::select('id', 'horario')->where('activo', true)->whereIn('id', $asignados)->orderBy('horario', 'asc')->get(),
        ]);
        return $this->response();
    }

    public function storeHorarios() {
        $this->validation_rules = [
            "movie_id" => "required|integer|exists:App\Pelicula,id",
            "horario" => "required|array|min:1",
        ];
        if (!$this->validator()) {
            return $this->response();
        }
        $horarios = $this->horario;
        PeliculasTurno::where('movie_id', $this->movie_id)->delete();
        foreach ($horarios as $id_horario) {
            $turno = new PeliculasTurno();
            $turno->movie_id = $this->movie_id;
            $turno->turno_id = $id_horario;
            $turno->save();
        }
        return $this->response(true);
    }

    public function delete($id) {
        $pelicula = Pelicula::where('id', $id)->first();
        if($pelicula === null) {
            $this->handleError(["general" => "No existe la película."]);
            return $this->response();
        }
        if ($pelicula->delete()) {
            $this->setDataResponse(true);
        } else {
            $this->handleError(["general" => "Ocurrio un problema al eliminar en la base de datos."]);
            return $this->response();
        }
        return $this->response();
    }

    public function visibility($id) {
        $pelicula = Pelicula::where('id', $id)->first();
        if($pelicula === null) {
            $this->handleError(["general" => "No existe la película."]);
            return $this->response();
        }
        $pelicula->activo = ($pelicula->activo) ? false : true;
        if ($pelicula->save()) {
            $this->setDataResponse(true);
        } else {
            $this->handleError(["general" => "Ocurrio un problema al modificar en la base de datos."]);
            return $this->response();
        }
        return $this->response();
    }

    public function update() {
        $this->validation_rules = [
            "id" => "required|integer|exists:App\Pelicula,id",
            "nombre" => "required|string",
            "fecha_publicacion" => "required|date|date_format:Y-m-d",
            "activo" => "required",
        ];
        if (!$this->validator()) {
            return $this->response();
        }
        $campos = $this->request->all();
        $pelicula = Pelicula::where('id', $this->id)->first();
        unset($campos['imagen']);

        if($this->request->hasFile('imagen') && !is_null($this->request->file('imagen'))) {
            $archivo = basename($this->request->file('imagen')->store('movies', 'public'));
            $campos['imagen'] = $archivo;
        }
        $pelicula->fill($campos);
        if ($pelicula->save()) {
            $this->setDataResponse(true);
        } else {
            $this->handleError(["general" => "Ocurrio un problema al guardar en la base de datos."]);
            return $this->response();
        }
        return $this->response();
    }

    public function store()
    {
        $this->validation_rules = [
            "nombre" => "required|string",
            "fecha_publicacion" => "required|date|date_format:Y-m-d",
            "imagen" => "required|image",
            "activo" => "required",
        ];
        if (!$this->validator()) {
            return $this->response();
        }
        $campos = $this->request->all();
        $pelicula = new Pelicula();
        unset($campos['imagen']);
        $pelicula->user_id = Auth::id();
        if($this->request->hasFile('imagen')) {
            $archivo = basename($this->request->file('imagen')->store('movies', 'public'));
            $campos['imagen'] = $archivo;
        }
        $pelicula->fill($campos);
        if ($pelicula->save()) {
            $this->setDataResponse(true);
        } else {
            $this->handleError(["general" => "Ocurrio un problema al guardar e la base de datos."]);
            return $this->response();
        }
        return $this->response();
    }
}
