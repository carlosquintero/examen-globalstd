import axios from 'axios'

export const GlobalsMixin = {
	methods: {
		$logout: function () {
			console.log('cerrar sesión')
			axios.get(`${this.$root.$apiURL}auth/logout`, {
                headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
            })
			this.$store.commit({
				type: 'setUser',
				logged: false,
				info: { name: '', email: '' }
			})
			localStorage.removeItem('token')
			this.$router.push({ name: 'login' })
		},
		$toast: function (tipo, mensaje, position) {
			//  'top', 'top-start', 'top-end', 'center', 'center-start', 'center-end', 'bottom', 'bottom-start', or 'bottom-end'
			const Toast = this.$swal.mixin({
				toast: true,
				position: position,
				showConfirmButton: false,
				timer: 3000,
				timerProgressBar: true,
				onOpen: (toast) => {
					toast.addEventListener('mouseenter', this.$swal.stopTimer)
					toast.addEventListener('mouseleave', this.$swal.resumeTimer)
				}
			})
			Toast.fire({
				icon: tipo,
				title: mensaje
			})
		},
	},
	computed: {
		
	},
	watch: {
		
	}
}
