import Vue from 'vue'
import store from '../store'
import router from '../router'
import axios from 'axios'

class InitController {

    static start() {
        this.checkAuth()
    }

    static checkAuth() {
        let token = localStorage.getItem('token')
        if (token) {
            axios.get(`${Vue.prototype.$apiURL}auth/get`, {
                headers: { Authorization: `Bearer ${token}` }
            }).then(res => {
                if (res.data) {
                    store.commit({
                        type: 'setUser',
                        logged: true,
                        info: { name: res.data.data.name, email: res.data.data.email }
                    })
                } else {
                    this.logout()
                }
            }).catch((res) => {
                console.log('no sesion', res)
                this.logout()
            })
        }
    }

    static logout() {
        store.commit({
            type: 'setUser',
            logged: false,
            info: { name: '', email: '' }
        })
        localStorage.removeItem('token')
        router.push({ name: 'login' })
    }
}

export default InitController;