import VueRouter from 'vue-router'

import Home from '../pages/Home'
import Login from '../pages/Login'
import Administradores from '../pages/Administradores'
import Turnos from '../pages/Turnos'
import Movies from '../pages/Movies'

const rutas = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: { needLogin: true }
    },
    {
        path: '/movies',
        name: 'movies',
        component: Movies,
        meta: { needLogin: true }
    },
    {
        path: '/turnos',
        name: 'turnos',
        component: Turnos,
        meta: { needLogin: true }
    },
    {
        path: '/administradores',
        name: 'administradores',
        component: Administradores,
        meta: { needLogin: true }
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: { needLogin: false }
    }
]

const router = new VueRouter({
    mode: 'hash',
    routes: rutas
})

router.beforeEach((to, from, next) => {
    // Redireccionar si la página esta protegida y no tiene un token. TODO: consultar que el token sea válido por medio de un api call
    if (to.matched.some(record => record.meta.needLogin) && !localStorage.getItem('token')) {
        next('/login');
    } else {
        next();
    }
});

export default router
