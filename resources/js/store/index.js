import Vuex from 'vuex'
import Vue from 'vue'
import GeneralState from './modules/GeneralState'

Vue.use(Vuex)

export default new Vuex.Store({
	modules: {
		general: GeneralState
	}
})
