export default {
	state: {
		user: { logged: false, info: { name: 'Usuario', email: '' } }
	},
	mutations: {
		setUser (state, payload) {
			delete payload.type
			state.user = payload
		}
	},
	actions: {},
	getters: {
		getUser (state) {
			return state.user
		}
	}
}
