/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue'
import 'es6-promise/auto'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import store from './store'
import router from './router'
import { BootstrapVue, BSidebar } from 'bootstrap-vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueSweetalert2 from 'vue-sweetalert2'
import InitController from './utils/InitController'
import bus from './bus'

library.add(fas)

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.component('b-sidebar', BSidebar)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(VueSweetalert2)

// mixins
import { GlobalsMixin } from './mixins/GlobalsMixin'
Vue.mixin(GlobalsMixin)

Vue.config.productionTip = false
Vue.prototype.$bus = bus;
Vue.prototype.$apiURL = process.env.NODE_ENV !== 'production' ? 'http://127.0.0.1:8000/api/' : 'http://127.0.0.1:8000/api/'

// revisar sesión del usuario
InitController.start()

new Vue({
    el: '#app',
    router,
    store
})
