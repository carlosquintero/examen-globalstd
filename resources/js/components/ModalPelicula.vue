<template>
    <div>
        <b-modal
            id="ModalPelicula"
            centered
            no-close-on-backdrop
            no-close-on-esc
            :title="!editMode ? 'Agregar película' : 'Editar película'"
            @hide="clearModel"
            :hide-footer="true"
        >
            <b-form @submit.prevent="submit">
                <b-alert :show="errorGeneralStatus" variant="danger">{{ errorGeneral }}</b-alert>
                <b-form-group>
                    <label>Nombre</label>
                    <b-form-input placeholder="Nombre" :disabled="busy" :state="errorNombreStatus" v-model="model.nombre" trim required />
                    <b-form-invalid-feedback :state="errorNombreStatus">
                        {{ errorNombre }}
                    </b-form-invalid-feedback>
                </b-form-group>
                <b-form-group>
                    <label>Fecha de publicación</label>
                    <b-form-datepicker v-model="model.fecha_publicacion" :state="errorFechaStatus" :value-as-date="false" :disabled="busy" placeholder="Selecciona una fecha" autocomplete="off" required />
                    <b-form-invalid-feedback :state="errorFechaStatus">
                        {{ errorFecha }}
                    </b-form-invalid-feedback>
                </b-form-group>
                <b-form-group>
                    <label>Imagen</label>
                    <b-form-file ref="imagen" v-model="model.imagen" :state="errorImagenStatus" :disabled="busy" accept="image/*" placeholder="Selecciona un archivo" :required="!editMode" />
                    <b-form-invalid-feedback :state="errorImagenStatus">
                        {{ errorImagen }}
                    </b-form-invalid-feedback>
                    <b-form-text id="input-live-help">{{ !editMode ? 'Puedes arrastrar y soltar tu imagen.' : 'Solamente si deseas cambiar la imagen.' }}</b-form-text>
                </b-form-group>
                <b-form-checkbox v-model="model.activo" switch>
                    Activo
                </b-form-checkbox>
                <div class="text-right">
                    <b-button type="submit" :disabled="busy" variant="dark">{{ !editMode ? 'Agregar' : 'Editar' }}</b-button>
                </div>
            </b-form>
        </b-modal>
    </div>
</template>

<script>
    import axios from 'axios'

    export default {
        name: 'ModalPelicula',
        data() {
            return {
                busy: false,
                editMode: false,
                model: {
                    id: 0,
                    nombre: '',
                    fecha_publicacion: new Date().toISOString().slice(0,10),
                    imagen: [],
                    activo: false
                },
                errors: [],
            }
        },
        computed: {
            errorGeneralStatus() {
                return this.errors && this.errors.general ? true : false
            },
            errorGeneral() {
                return this.errors && this.errors.general ? this.errors.general : ''
            },
            errorNombreStatus() {
                return this.errors && this.errors.nombre ? false : null
            },
            errorNombre() {
                return this.errors && this.errors.nombre ? this.errors.nombre[0] : ''
            },
            errorImagenStatus() {
                return this.errors && this.errors.imagen ? false : null
            },
            errorImagen() {
                return this.errors && this.errors.imagen ? this.errors.imagen[0] : ''
            },
            errorFechaStatus() {
                return this.errors && this.errors.fecha_publicacion ? false : null
            },
            errorFecha() {
                return this.errors && this.errors.fecha_publicacion ? this.errors.fecha_publicacion[0] : ''
            }
        },
        created() {
            this.$bus.$on('openModalPelicula', (row) => {
                if (Object.keys(row).length > 0) {
                    this.editMode = true
                    Object.entries(row).forEach((entry) => {
                        const [key, value] = entry
                        if (key !== 'imagen' && typeof this.model[key] !== 'undefined') {
                            this.model[key] = value
                        }
                    })
                }
                this.$bvModal.show("ModalPelicula")
            })
        },
        methods: {
            clearModel() {
                this.editMode = false
                this.model = {
                    id: 0,
                    nombre: '',
                    fecha_publicacion: new Date().toISOString().slice(0,10),
                    imagen: [],
                    activo: false
                };
                this.errors = []
            },
            prepareModel() {
                let model = new FormData()
                Object.entries(this.model).forEach((entry) => {
                    const [key, value] = entry
                    // console.log(key, value)
                    model.append(key, value)
                })
                return model
            },
            submit() {
                this.busy = true
                this.errors = []
                const model = this.prepareModel()
                const instance = (this.editMode) ? axios.post(`${this.$root.$apiURL}movies/update/${this.model.id}`, model, {
                    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
                }) : axios.post(`${this.$root.$apiURL}movies/store`, model, {
                    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
                })
                instance.then(res => {
                    if (res.data) {
                        this.$root.$toast('success', !this.editMode ? 'Pelicula agregada!' : 'Pelicula editada!', 'top-end')
                        this.$bvModal.hide("ModalPelicula")
                        this.$bus.$emit('loadMovies')
                    } else {
                        this.$swal('Ocurrio un error desconocido.')
                    }
                }).catch(res => {
                    if (!res.response.data.errors) {
                        this.$swal('Error desconocido')
                    } else {
                        this.errors = res.response.data.errors
                        console.log('errores', this.errors)
                    }
                }).finally(() => {
                    this.busy = false
                })
            }
        }
    }

</script>
